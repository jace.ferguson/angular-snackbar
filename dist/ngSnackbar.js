/**
 * Created by a13532 on 8/2/2016.
 */
angular.module('ng-snackbar', []);
/**
 * Created by a13532 on 8/2/2016.
 */

angular.module('ng-snackbar')
    .constant('snackbarTemplate', "<div class=\"snackbar\">{{visible.content}} <button class=\"dismiss-button\" ng-click=\"dismiss()\"><span></span></button></div>")
    .controller('SnackbarCtrl', ["$scope", "$element", "$attrs", "$rootScope", "$timeout", "$compile", "snackbarTemplate", function($scope, $element, $attrs, $rootScope, $timeout, $compile, snackbarTemplate){
        $scope.snackbars = [];
        $scope.visible = null;
        $scope.visibleElement = null;
        $scope.currentTimeout = null;
        $rootScope.$on('ngSnackbarShow', function($event, snackbar){
            $scope.snackbars.unshift(snackbar);
            $scope.showSnackbar();
        });

        $scope.showSnackbar = function(){
            if(!$scope.snackbars.length || $scope.visible != null){
                return;
            }
            var sb = $scope.snackbars.pop();
            if(angular.isNumber(sb.duration) && sb.duration > 0){
                $scope.currentTimeout = $timeout(function(){
                    $scope.currentTimeout = null;
                   $scope.dismiss();
                }, sb.duration);
            }
            $scope.visible = sb;
            $scope.visibleElement = $compile(snackbarTemplate)($scope);
            angular.element($element.children()[0]).empty().append($scope.visibleElement);
            $scope.visibleElement.addClass('snackbar-opened');
        };

        $scope.dismiss = function(){
            if($scope.currentTimeout != null){
                $timeout.cancel($scope.currentTimeout);
            }

            $scope.visibleElement.remove();

            $scope.visible = null;
            $scope.visibleElement = null;
            $scope.currentTimeout = null;
            $scope.showSnackbar();
        };
    }])
    .directive('ngSnackbar', function(){
        return {
            restrict: 'AE',
            controller: 'SnackbarCtrl',
            templateUrl: 'src/views/snackbarContainer.html',
            scope: {

            }
        }
    });
/**
 * Created by a13532 on 8/2/2016.
 */
angular.module('ng-snackbar')
    .service('Snackbar', ["$rootScope", function Snackbar($rootScope){
        var ng = angular;
        var id = 0;

        var activeSnackbars = [];

        var defaults = {
            id: null,
            content: "",
            style: "",
            duration: 5000,
            htmlAllowed: false,
            onDismiss: ng.noop,
            actionText: false,
            onAction: ng.noop

        };

        this.create = function(content, duration){
            var snackbar = ng.extend({}, defaults);
            if(ng.isString(content)){
                snackbar['content'] = content;
                if(ng.isNumber(duration)){
                    snackbar['duration'] = duration;
                }
            }
            else if(ng.isObject(content)){
                snackbar = ng.extend(snackbar, content);
            }
            snackbar.id = ++id;
            $rootScope.$broadcast('ngSnackbarShow', snackbar);
            return snackbar.id;
        };

        return this;
    }]);
angular.module('ng-snackbar').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('src/views/snackbarContainer.html',
    "<div class=\"snackbar-container\"></div>"
  );

}]);
