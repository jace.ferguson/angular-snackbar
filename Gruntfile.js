// Generated on 2014-02-06 using generator-angular 0.7.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        projectPaths: {
            // configurable paths
            app: 'src',
            tmp: '.tmp',
            dist: './dist'
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp'
                    ]
                }]
            }
        },

        concat: {
            dist: {
                src: ['<%= projectPaths.app %>/angularSnackbarModule.js','<%= projectPaths.app %>/*.js'],
                dest: '<%= projectPaths.tmp %>/ngSnackbar.js'
            }
        },

        ngAnnotate: {
            dist: {
                files: {
                    '<%= projectPaths.tmp %>/ngSnackbar.js': ['<%= projectPaths.tmp %>/ngSnackbar.js']
                }
            }
        },

        copy: {
            dist: {
                files: {
                    '<%= projectPaths.dist %>/ngSnackbar.js': ['<%= projectPaths.tmp %>/ngSnackbar.js']
                }
            }
        },


        uglify: {
            options:{
                sourceMap:  true,
                sourceMapIncludeSources: true
            },
            dist: {
                files: {
                    '<%= projectPaths.dist %>/ngSnackbar.min.js': ['<%= projectPaths.tmp %>/ngSnackbar.js']
                }
            }

        },
        less: {
            test:{
                options: {
                    paths: ["<%= yeoman.app %>/less"]
                },
                files: {
                    '<%= projectPaths.dist %>/css/snackbar.css': "<%= projectPaths.app %>/less/snackbar.less"
                }
            },
            dist: {
                files: {
                    '<%= projectPaths.dist %>/css/snackbar.css': "<%= projectPaths.app %>/less/snackbar.less"
                }
            }
        },

        ngtemplates: {
            compile: {
                src: '<%= projectPaths.app %>/views/*.html',
                dest: '<%= projectPaths.app %>/templates.js',
                options: {
                    htmlmin: {
                        collapseBooleanAttributes: true,
                        collapseWhitespace: true,
                        removeAttributeQuotes: false,
                        removeComments: true, // Only if you don't use comment directives!
                        removeEmptyAttributes: true,
                        removeRedundantAttributes: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true
                    },
                    module: 'ng-snackbar'
                }
            }
        }

    });


    grunt.registerTask('build', [
        'clean:dist',
        'less:dist',
        'ngtemplates:compile',
        'concat:dist',
        'ngAnnotate:dist',
        'copy:dist',
        'uglify:dist'
    ]);

    grunt.registerTask('lesser', [
        'less:dist'
    ]);


};
