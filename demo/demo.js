/**
 * Created by a13532 on 8/2/2016.
 */

angular.module('DemoModule', ['ng-snackbar'])
    .controller('DemoCtrl', function(Snackbar, $timeout, $scope){
        $scope.counter = 0;
        $timeout(function(){
            Snackbar.create("This is a test", 0);
        }, 0);

        $scope.doAddSnackbar = function(){
            Snackbar.create("This is a test " + $scope.counter++);
        }

    });
