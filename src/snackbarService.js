/**
 * Created by a13532 on 8/2/2016.
 */
angular.module('ng-snackbar')
    .service('Snackbar', function Snackbar($rootScope){
        var ng = angular;
        var id = 0;

        var activeSnackbars = [];

        var defaults = {
            id: null,
            content: "",
            style: "",
            duration: 5000,
            htmlAllowed: false,
            onDismiss: ng.noop,
            actionText: false,
            onAction: ng.noop

        };

        this.create = function(content, duration){
            var snackbar = ng.extend({}, defaults);
            if(ng.isString(content)){
                snackbar['content'] = content;
                if(ng.isNumber(duration)){
                    snackbar['duration'] = duration;
                }
            }
            else if(ng.isObject(content)){
                snackbar = ng.extend(snackbar, content);
            }
            snackbar.id = ++id;
            $rootScope.$broadcast('ngSnackbarShow', snackbar);
            return snackbar.id;
        };

        return this;
    });