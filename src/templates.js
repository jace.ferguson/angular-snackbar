angular.module('ng-snackbar').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('src/views/snackbarContainer.html',
    "<div class=\"snackbar-container\"></div>"
  );

}]);
